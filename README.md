Modern computer vision methods make heavy use of algorithms that automatically
determine useful image features and tune algorithm parameters to maximize
accuracy. This lecture will introduce basic formulations of classification and
regression prediction tasks and architectures for performing these tasks based
on linear prediction, decision trees and “deep” artificial neural networks.
Students will gain a high-level understanding of how these architectures are
automatically optimized from training examples and how image features can be
selected that provide predictive power. Students will see several case-studies
that use these techniques for specific big image data applications and will be
introduced to several software packages that implement these methods and make
them accessible to end users who lack programming expertise.

Lecture Topics: 

Classification and regression, support vector machines, randomized decision
forests, feature selection, convolutional neural nets, regularization, K-means
clustering, Gaussian mixture models.  

Key Concepts Learned:

Students will learn standard formalisms for specifying machine learning tasks
and basic mathematical and algorithmic tools for learning predictors from data. 

References:

Machine learning in cell biology – teaching computers to recognize phenotypes
Christoph Sommer, Daniel W. Gerlich
J Cell Sci 2013 126: 5529-5539; doi: 10.1242/jcs.123604
http://jcs.biologists.org/content/joces/126/24/5529.full.pdf

The Elements of  Statistical Learning: Data Mining, Inference, and Prediction.
T. Hastie, R. Tibshirani, J. Friedman
http://statweb.stanford.edu/~tibs/ElemStatLearn/

Machine learning: a probabilistic perspective
Kevin Murphy
https://www.cs.ubc.ca/~murphyk/MLbook/

